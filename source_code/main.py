import pymysql
#from app import app
#from db_config import mysql
from flask import jsonify
from flask import flash, request
from werkzeug import generate_password_hash, check_password_hash

from flask import Flask

#import logging

#logging.basicConfig(filename='crudapp.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')
#logging.basicConfig(level=logging.INFO)

# Imports the Google Cloud client library
from google.cloud import logging

app = Flask(__name__)


#######################################

#from app import app
from flaskext.mysql import MySQL

mysql = MySQL()

# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'root'
app.config['MYSQL_DATABASE_DB'] = 'mydb'
app.config['MYSQL_DATABASE_HOST'] = '127.0.0.1'
mysql.init_app(app)

###########################################


def write_entry(logger_name):
    """Writes log entries to the given logger."""
    logging_client = logging.Client()

    # This log can be found in the Cloud Logging console under 'Custom Logs'.
    logger = logging_client.logger(logger_name)

    # Make a simple text log
    logger.log_text('Hello, world!')

    # Simple text log with severity.
    logger.log_text('Goodbye, world!', severity='ERROR')

    # Struct log. The struct can be any JSON-serializable dictionary.
    logger.log_struct({
        'name': 'King Arthur',
        'quest': 'Find the Holy Grail',
        'favorite_color': 'Blue'
    })

    print('Wrote logs to {}.'.format(logger.name))


###############################################


@app.route('/add', methods=['POST'])
def add_user():
        #logging.info("function Entry: add_user")
        try:
                _json = request.json
                _name = _json['name']
                _email = _json['email']
                _password = _json['pwd']
                # validate the received values
                if _name and _email and _password and request.method == 'POST':
                        #do not save password as a plain text
                        _hashed_password = generate_password_hash(_password)
                        # save edits
                        sql = "INSERT INTO ABC1(user_name, user_email, passwd) VALUES(%s, %s, %s)"
                        data = (_name, _email, _hashed_password,)
                        conn = mysql.connect()
                        cursor = conn.cursor()
                        cursor.execute(sql, data)
                        conn.commit()
                        resp = jsonify('User added successfully!')
                        resp.status_code = 200
                        return resp
                else:
                        return not_found()
        except Exception as e:
                print(e)
        finally:
                cursor.close()
                conn.close()

@app.route('/users')
def users():
        logging.info("function Entry: users")
        try:
                conn = mysql.connect()
                cursor = conn.cursor(pymysql.cursors.DictCursor)
                cursor.execute("SELECT * FROM ABC1")
                rows = cursor.fetchall()
                resp = jsonify(rows)
                resp.status_code = 200
                #logging.info("/users : 200 OK")
		write_entry("usersfun")
                return resp
        except Exception as e:
                #logging.info("users/ : Exception")
                print(e)
        finally:
                cursor.close()
                conn.close()

@app.route('/user/')
def user(id):
        try:
                conn = mysql.connect()
                cursor = conn.cursor(pymysql.cursors.DictCursor)
                cursor.execute("SELECT * FROM ABC1 WHERE username=%s", ABC)
                row = cursor.fetchone()
                resp = jsonify(row)
                resp.status_code = 200
                return resp
        except Exception as e:
                print(e)
        finally:
                cursor.close()
                conn.close()

@app.route('/update', methods=['POST'])
def update_user():
        try:
                _json = request.json
                _id = _json['id']
                _name = _json['name']
                _email = _json['email']
                _password = _json['pwd']
                # validate the received values
                if _name and _email and _password and _id and request.method == 'POST':
                        #do not save password as a plain text
                        _hashed_password = generate_password_hash(_password)
                        # save edits
                        sql = "UPDATE ABC1 SET username=%s, user_email=%s, passwd=%s WHERE user_id=%s"
                        data = (_name, _email, _hashed_password, _id,)
                        conn = mysql.connect()
                        cursor = conn.cursor()
                        cursor.execute(sql, data)
                        conn.commit()
                        resp = jsonify('User updated successfully!')
                        resp.status_code = 200
                        return resp
                else:
                        return not_found()
        except Exception as e:
                print(e)
        finally:
                cursor.close()
                conn.close()

@app.route('/delete/')
def delete_user(id):
        try:
                conn = mysql.connect()
                cursor = conn.cursor()
                cursor.execute("DELETE FROM tbl_user WHERE user_id=%s", (id,))
                conn.commit()
                resp = jsonify('User deleted successfully!')
                resp.status_code = 200
                return resp
        except Exception as e:
                print(e)
        finally:
                cursor.close()
                conn.close()

@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Not Found: ' + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 404

    return resp

if __name__ == "__main__":
    app.run(host = '0.0.0.0',port=8080)

